var express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    cookieSession = require('cookie-session'),
    serveStatic = require('serve-static'),
    expressValidator = require('express-validator'),
    passport = require('passport'),
    crypto = require('crypto'),
    flash = require('connect-flash'),
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    session = require('express-session'),
    path = require('path'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    ObjectID = require('mongodb').ObjectID;

var routes = require('./src/routes/routes');
// var users = require('./src/routes/users');

var LocalStrategy = require('passport-local').Strategy;

// templating
var handlebars = require('express-handlebars').create({
    layoutsDir: "views/layouts",
    partialsDir: "views/partials",
    defaultLayout: 'main'
});

var app = express();

// Express and Passport Session
app.use(session({
    secret: "secretAppAdrienSecret",
    proxy: true,
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

// app.use(morgan('dev')); // log every request to the console
// mongoose.connect('mongodb://127.0.0.1:27017/samplenodejsapp');
mongoose.connect('mongodb://user:pwd@ds143767.mlab.com:43767/newo');

app.use(cookieParser('SampleNode.jsApp')); //read cookies
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({
    extended: true
}));
app.set('views', __dirname + '/views'); // set template directory
app.set('view engine', 'handlebars'); //set view engine to handlebars
app.engine('handlebars', handlebars.engine);
app.use(expressValidator());

app.use(serveStatic('./public')); //serve static files from public directory
app.use(flash()); //used to display flash messages from sessions


app.use(favicon(__dirname + '/public/img/favicon.ico'));


require('./src/middleware/passportNew')(passport);
require('./src/routes/routes')(app, passport); // include routes


var port = process.env.PORT || 3000;

app.listen(port, function() {
    console.log('Magic begins on port '+port+'!');
});
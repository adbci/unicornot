var Comment = require('../model/comment');
var Idea = require('../model/idea');
var User = require('../model/user');
// Require controller modules
var idea_controller = require('../controller/ideaController');
var comment_controller = require('../controller/commentController');
var utils_controller = require('../controller/utilsController');
var async = require('async');

var multer = require('multer');
var multerS3 = require('multer-s3');
var bodyParser = require('body-parser');
var fs = require('fs');
var aws = require('aws-sdk');

module.exports = function (app, passport) {

    /* ###############################################
     AMAZON S3 UPLOAD TEST SECTION
     ############################################### */


    app.get('/test', function (req, res) {

        res.render('test');

    });


    var s3 = new aws.S3({accessKeyId: 'AKIAIAMFRGVG4CEJ75HA',
        secretAccessKey: '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',
        signatureVersion: 'v4'
    });

    var uploads3 = multer({
        storage: multerS3({
            s3: s3,
            bucket: 'bucketunicornot',
            acl: 'public-read',
            key: function (req, file, cb) {
                // cb(null, Date.now().toString())
                console.log("#00 :"+req.files.originalname);
                console.log("#000 :"+file.originalname);
                nomImage = file.originalname;
                cb(null, nomImage)
            }
        })
    });

    /*
     app.post('/upload', uploads3.array('photos', 1), function (req, res, next) {
     //reference results that can be stored in database for example in req.files
     res.send('Successfully uploaded ' + req.files.length + ' files!')
     });
     */


    /* ###############################################
     FIN AMAZON S3 UPLOAD TEST SECTION
     ############################################### */






    // PARAM UPLOAD MULTER

    var storage = multer.diskStorage({
        destination: function (request, file, callback) {
            callback(null, './public/img/uploads/');
        },
        filename: function (request, file, callback) {
            console.log(file);
            callback(null, file.originalname)
        }
    });
    var upload = multer({storage: storage});


    // normal routes ===============================================================

    /* GET home page. */
    app.get('/', utils_controller.getHomepage);

    // GET request for last idea displayed when user first visits and isn't logged
    app.get('/view', idea_controller.last_idea);

    app.get('/view2', idea_controller.last_idea_minus_1);

    // SECTION IDEAS USING CONTROLLER
    // GET request for list of all ideas items
    app.get('/index', ensureAuthenticated, idea_controller.ideas_listing);

    // PROFILE SECTION =========================
    app.get('/profile', ensureAuthenticated, utils_controller.getProfile);

    // SECTION COMMENTS USING CONTROLLER
    app.get('/allComments', ensureAuthenticated, comment_controller.getAllComments);

    // GET request for upvoting an idea
    app.get('/upvote/:id', ensureAuthenticated, idea_controller.idea_upvoting);

    // GET request for upvoting an idea
    app.get('/downvote/:id', ensureAuthenticated, idea_controller.idea_downvoting);

    // GET request for list of all ideas items
    app.get('/listIdeas', ensureAuthenticated, idea_controller.ideas_listing);

    // displayng the form to add an idea
    app.get('/addIdea', ensureAuthenticated, idea_controller.ideas_add);

    // displayng most commented ideas
    app.get('/brain', ensureAuthenticated, idea_controller.idea_filtered_by_comments);

    // displayng most commented ideas
    app.get('/buzz', ensureAuthenticated, idea_controller.idea_filtered_by_comments_and_rating);

    // displayng best rated ideas
    app.get('/top', ensureAuthenticated, idea_controller.idea_filtered_by_rating);

    // processing the form
    // app.post('/addIdea', upload.single('ideaImage'), ensureAuthenticated, idea_controller.ideas_add_process);
    app.post('/addIdea', uploads3.array('ideaImage', 1),ensureAuthenticated, idea_controller.ideas_add_process);

    // GET request for one idea
    app.get('/idea/:id', ensureAuthenticated, idea_controller.idea_details);

    // SECTION COMMENTS USING CONTROLLER
    // processing the form
    app.post('/idea/:id', ensureAuthenticated, comment_controller.comment_add);

    // displaying the form
    // form is integrated to the detailed idea page

    // =============================================================================
    // AUTHENTICATE (FIRST LOGIN) ==================================================
    // =============================================================================

    // locally --------------------------------
    // LOGIN ===============================
    // show the login form
    app.get('/login', utils_controller.getLoginPage);


    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/index', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // SIGNUP =================================
    // show the signup form
    app.get('/signup', function (req, res) {
        res.render('signup', {
            message: req.flash('signupMessage')
        });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/index', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));


// ########### AUTH WITH GITHUB ##################
    app.get('/auth/github', passport.authenticate('github'));

    app.get('/auth/github/callback', passport.authenticate('github', {failureRedirect: '/login'}),
        function (req, res, accessToken) {
            res.redirect('/index');
        }
    );

// ########### AUTH WITH FACEBOOK ##################
    app.get('/auth/facebook', passport.authenticate('facebook', {scope: ['email']}));

    app.get('/auth/facebook/callback', passport.authenticate('facebook', {failureRedirect: '/login'}),
        function (req, res) {
            res.redirect('/index');
        });

// ########### AUTH WITH GOOGLE ##################
    app.get('/auth/google', passport.authenticate('google', {
        scope: [
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/plus.profile.emails.read']
    }));

    app.get('/auth/google/callback', passport.authenticate('google', {failureRedirect: '/login'}),
        function (req, res) {
            res.redirect('/index');
        });

// ########### AUTH WITH TWITTER ##################
    app.get('/auth/twitter', passport.authenticate('twitter'));

    app.get('/auth/twitter/callback', passport.authenticate('twitter', {failureRedirect: '/login'}),
        function (req, res) {
            res.redirect('/index');
        });

    // LOGOUT USING CONTROLLER
    app.get('/logout', utils_controller.logout);


//################################################
//######### AUTHORIZATION ########################
//################################################

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

    // locally --------------------------------
    // send to local system to do the authentication
    app.get('/connect/local', utils_controller.loginLocal);

    // handle the callback after local system has authorized the user
    app.post('/connect/local', utils_controller.callbackLocal);

    /*
     app.get('/connect/local', function (req, res) {
     res.render('signupLocal', {message: req.flash('loginMessage')});
     });


     app.post('/connect/local',
     passport.authenticate('local-signup', {
     successRedirect: '/profile', // redirect to the secure profile section
     failureRedirect: '/connect/local', // redirect back to the signup page if there is an error
     failureFlash: true // allow flash messages
     }));
     */



    // facebook -------------------------------

    // send to facebook to do the authentication
    app.get('/connect/facebook', passport.authorize('facebook', {scope: 'email'}));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback',
        passport.authorize('facebook', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));

    // twitter --------------------------------

    // send to twitter to do the authentication
    app.get('/connect/twitter', passport.authorize('twitter', {scope: 'email'}));

    // handle the callback after twitter has authorized the user
    app.get('/connect/twitter/callback',
        passport.authorize('twitter', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // github --------------------------------

    // send to github to do the authentication
    app.get('/connect/github', passport.authorize('github', {scope: 'email'}));

    // handle the callback after github has authorized the user
    app.get('/connect/github/callback',
        passport.authorize('github', {
            successRedirect: '/profile',
            failureRedirect: '/'
        }));


    // google ---------------------------------

    // send to google to do the authentication
//        app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));
    app.get('/connect/google', passport.authorize('google', {
        scope: [
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/plus.profile.emails.read']
    }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback', utils_controller.callbackGoogle);

    /*
     app.get('/connect/google/callback',
     passport.authorize('google', {
     successRedirect: '/profile',
     failureRedirect: '/'
     }));
     */

// ############### UNLINK ###################

    // local -----------------------------------
    app.get('/unlink/local', utils_controller.unlinkLocal);

    // facebook -------------------------------
    app.get('/unlink/facebook', utils_controller.unlinkFacebook);

    // google ---------------------------------
    app.get('/unlink/google', utils_controller.unlinkGoogle);

    // github -------------------------------
    app.get('/unlink/github', utils_controller.unlinkGithub);

    // twitter --------------------------------
    app.get('/unlink/twitter', utils_controller.unlinkTwitter);


//################################################
//######### AUTHENTICATION #####################
//################################################

// Simple middleware to ensure user is authenticated.
// Use this middleware on any resource that needs to be protected.
// If the request is authenticated (typically via a persistent login session),
// the request will proceed.  Otherwise, the user will be redirected to the
// login page.
    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            // req.user is available for use here
            return next();
        }
        // denied. redirect to login
        res.redirect('/login')
    }

};
// /model/comment.js
var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;
var User = require('../model/user');
var Idea = require('../model/idea');
var Comments = require('../model/comment');
var Comment = require('../model/comment');

var commentSchema = mongoose.Schema({
    name: {type: mongoose.Schema.Types.String, ref: 'User'},
    idUser: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    idIdea: {type: mongoose.Schema.Types.ObjectId, ref: 'Idea'},
    photo: String,
    title     : String,
    body      : String,
    dateComment      : {type: Date, default: Date.now}
});

/*
var commentSchema = mongoose.Schema({
    name: {type: mongoose.Schema.Types.String, ref: 'User'},
    idUser: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    idIdea: {type: mongoose.Schema.Types.ObjectId, ref: 'Idea'},
    title     : String,
    body      : String,
    dateComment      : {type: Date, default: Date.now}
});
 */

// Virtual for this comment instance URL
commentSchema
    .virtual('url')
    .get(function () {
        return '/comment/'+this._id;
    });

commentSchema
    .virtual('idComment')
    .get(function () {
        return this._id;
    });

commentSchema
    .virtual('dateComment_formatted')
    .get(function () {
        return moment(this.dateComment).format('MMMM, Do YYYY');
    });

module.exports = mongoose.model("Comments", commentSchema);
// /model/idea.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');
var Comment = require('../model/comment');
var Comments = require('../model/comment');
var User = require('../model/user');
var Categories = require('../model/category');
var Topers = require('../model/topers');
var Flopers = require('../model/flopers');

var ideaSchema = mongoose.Schema({
    ideaSubject: String,
    ideaExcerpt: String,
    ideaDescription: String,
    ideaImage: String,
    name: {type: mongoose.Schema.Types.String, ref: 'User'},
    idAuthor: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    // category: [{type: mongoose.Schema.Types.ObjectId, ref: 'Categories'}],
    category: String,
    dateAddedIdea: {type: Date, default: Date.now},
//    comments:[{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],
    comments: [Comments],
    meta: {
        top: {type: Number, default: 0},
        flop: {type: Number, default: 0}
    },
    toper: [{type: mongoose.Schema.Types.ObjectId, ref: 'Topers'}],
    floper: [{type: mongoose.Schema.Types.ObjectId, ref: 'Flopers'}]
});

// Virtual for this idea instance URL
ideaSchema
    .virtual('url')
    .get(function () {
        return '/idea/' + this._id;
    });

ideaSchema
    .virtual('id')
    .get(function () {
        return this._id;
    });

ideaSchema
    .virtual('date_formatted')
    .get(function () {
        return moment(this.dateAddedIdea).format('MMMM, Do YYYY');
    });



module.exports = mongoose.model("Idea", ideaSchema);

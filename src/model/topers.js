// /model/topers.js
var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;
var Ideas = require('../model/idea');

var toperSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    toperName: String
});

module.exports = mongoose.model("Topers", toperSchema);
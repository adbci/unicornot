// /model/flopers.js
var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;
var Ideas = require('../model/idea');

var floperSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    floperName: String
});

module.exports = mongoose.model("Flopers", floperSchema);
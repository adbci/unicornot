// /model/category.js
var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema;
var Categories = require('../model/category');
var Ideas = require('../model/idea');

var categoriesSchema = mongoose.Schema({
   // _id: mongoose.Schema.Types.ObjectId,
    categoryName: String
});

module.exports = mongoose.model("Categories", categoriesSchema);
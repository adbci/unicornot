var async = require('async');
var Comment = require('../model/comment');
var Comments = require('../model/comment');
var User = require('../model/user');
var Idea = require('../model/idea');
var ObjectID = require('mongodb').ObjectID;
var utilsController = require('../controller/utilsController');
var ideaController = require('../controller/ideaController');

exports.comment_add = function (req, res, next) {

    var myId = req.params.id;

    Idea.findById(myId, function (err, idea) {
        if (!err) {

            var commentsInfo = req.body;

            if (!commentsInfo.title || !commentsInfo.body) {
                console.log("Erreur champ vide");
                res.redirect('/idea/' + myId + '/?error=empty');
            }
            else {
                var title = req.body.title;
                var body = req.body.body;
                var date = new Date();

                var name = utilsController.getNameFromDataUser(req);
                var photo = utilsController.getPhotoFromDataUser(req);

                var customObjectId = new ObjectID();

                var newComment = new Comment()
                {
                    newComment._id = customObjectId,
                        newComment.name = name,
                        newComment.idUser = req.user.id,
                        newComment.photo = photo,
                        newComment.idIdea = myId,
                        newComment.title = title,
                        newComment.body = body,
                        newComment.dateComment = date
                }

                idea.comments.push({
                    _id: newComment._id,
                    name: newComment.name,
                    idUser: newComment.idUser,
                    idIdea: myId,
                    photo: photo,
                    title: newComment.title = title,
                    body: newComment.body = body,
                    dateComment: newComment.date = date
                });

                idea.save(function (err) {
                    console.log(idea.toString());
                });

                newComment.save(function (erreur, ideaInfo) {
                    console.log(ideaInfo);
                    if (erreur) {
                        res.redirect('/idea/' + myId + '?error=database');
//                        res.render('addIdea', {message: "Database error", type: "error"});
                    }
                    if (!erreur) {
                        res.redirect('/idea/' + myId + '?success');
                    }
//            res.render('ideas', {title: 'Ideas listing new version', idea_listing: list_ideas, user: req.user});
                })
            }
        }
    })
};


exports.countIdeaComments = function (req, res) {
    // console.log("IDEA #: " + req.params.id);
    var ideaToCheck = 'ObjectId("' + req.params.id + '")';
    // console.log("IDEA TO CHECK :" + ideaToCheck);

    Comments.aggregate([
        {
            $group: {
                _id: "$idIdea",
                num_comment: {$sum: 1}
            }
        }
    ])
        .exec(function (err, countComments) {
            if (err) {
                return next(err);
            }
            var found = false;
            for (var i = 0; i < countComments.length; i++) {
                if (results.countComment[i]._id == req.params.id) {
                    found = true;
                    var commentNumber = results.countComment[i].num_comment;
                    if (commentNumber < 2) {
                        var commentaire = " commentaire";
                    } else {
                        commentaire = " commentaires";
                    }
                }
                if (!found) {
                    commentNumber = 0;
                    commentaire = " commentaire";
                }
            }
        });
};


exports.getAllComments = function (req, res, next) {
    function countComments(idIdea) {
        // db.getCollection('ideas').findOne({},{comments: true}, {_id:'ObjectId("58a69a2250313a02f83c7f1b")'});
        db.getCollection('ideas').findOne({}, {comments: true}, {_id: idIdea})
            .exec(function (err, countComments) {
                if (err) {
                    console.log("Erreur counting comments");
                    return next(err);
                } else {
                    console.log(idIdea);
                    console.log(countComments);
                    alert(countComments);
                }
            });
    }

    Idea.find({})
        .exec(function (err, ideaComments) {
            if (err) {
                console.log("Erreur displaying comments");
                return next(err);
            }
            res.render('viewComments', {title: 'All comments', ideaComments: ideaComments, fs: {echo: countComments}});
        });

};
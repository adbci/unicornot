var async = require('async');
var mongoose = require('mongoose');
var Comment = require('../model/comment');
var Comments = require('../model/comment');
var Category = require('../model/category');
var toper = require('../model/topers');
var Floper = require('../model/flopers');
var Idea = require('../model/idea');
var ideaController = require('../controller/ideaController');
var utilsController = require('../controller/utilsController');
var aws = require('aws-sdk');

var multer = require('multer');
var multerS3 = require('multer-s3');
var path = require('path');

// ############################
// TESTS SECTION
// ############################



// ############################
// END TESTS SECTION
// ############################




/** ############################################
 ############## UPVOTING #################
 ############################################*/

exports.idea_upvoting = function (req, res, next) {
    var ideaId = req.params.id;
    Idea.findById(ideaId, function (err, idea) {
        if (!err) {

            idea.toper.push(
                {
                    _id: req.user.id
                });

            idea.meta.top++;
            idea.save(function (err) {
                console.log(idea.toString());
            });
            res.redirect('/idea/' + ideaId);
        }
    });
};


/** ############################################
 * ############## DOWNVOTING #################
 ############################################*/

exports.idea_downvoting = function (req, res, next) {
    var ideaId = req.params.id;
    Idea.findById(ideaId, function (err, idea) {
        if (!err) {

            idea.floper.push(
                {
                    _id: req.user.id
                });

            idea.meta.flop++;
            idea.save(function (err) {
                console.log(idea.toString());
            });
            res.redirect('/idea/' + ideaId);
        }
    });
};

// ############################
// Display last submitted idea
// ############################
exports.last_idea = function (req, res, next) {
    Idea.find().limit(1)
        .exec(function (err, last_idea) {
            if (err) {
                console.log("Erreur displayin last 1");
                return next(err);
            }
            //Successful, so render
            res.render('view', {title: 'Last submitted idea', idea_last: last_idea, user: req.user});
        });
};

// ############################
// Display last submitted idea minus 1
// ############################
exports.last_idea_minus_1 = function (req, res, next) {
    Idea.find().limit(1).skip(1)
        .exec(function (err, last_idea) {
            if (err) {
                console.log("Erreur displayin last 1 minus 1");
                return next(err);
            }
            //Successful, so render
            res.render('view2', {title: 'Last submitted idea -1', idea_last: last_idea, user: req.user});
        });
};


// ############################
// Display all ideas
// ############################
exports.ideas_listing = function (req, res, next) {
    Idea.find().sort({dateAddedIdea: -1})
        .exec(function (err, list_ideas) {
            if (err) {
                return next(err);
            }
            //Successful, so render
            res.render('ideas', {title: 'Last submitted ideas', idea_listing: list_ideas, user: req.user});
        });
};

// ################################
// Display a particular idea by id
// ################################
exports.idea_details = function (req, res, next) {

    async.parallel({
        idea_detail: function (callback) {
            Idea.find({'_id': req.params.id})
                .exec(callback);
        },
        countComment: function (callback) {
            console.log("IDEA #: " + req.params.id);
            var ideaToCheck = 'ObjectId("' + req.params.id + '")';
            console.log("IDEA TO CHECK :" + ideaToCheck);

            Comments.aggregate([
                {
                    $group: {
                        _id: "$idIdea",
                        num_comment: {$sum: 1}
                    }
                }
            ])
                .exec(callback);
        }
    }, function (err, results) {
        if (err) {
            return next(err);
        }

        var found = false;
        for (var i = 0; i < results.countComment.length; i++) {
            if (results.countComment[i]._id == req.params.id) {
                found = true;
                var commentNumber = results.countComment[i].num_comment;
                if (commentNumber < 2) {
                    var commentaire = " commentaire";
                } else {
                    commentaire = " commentaires";
                }
            }
            if (!found) {
                commentNumber = 0;
                commentaire = " commentaire";
            }
        }

        res.render('detailsIdea', {
            title: 'Details idea',
            idea_details: results.idea_detail,
            idea_comment_number: commentNumber + commentaire,
            user: req.user});

    })
};


// ################################
// Display ideas by filter
// ################################

// ################################
// Most commented ideas
// ################################

exports.idea_filtered_by_comments = function (req, res, next) {
    Idea.find({}).sort({comments: -1})
        .exec(function (err, list_ideas) {
            if (err) {
                return next(err);
            }
            res.render('ideas', {title: 'Most commented ideas', idea_listing: list_ideas, user: req.user, brain: true});
        });
};

// ################################
// Better rated ideas
// ################################

exports.idea_filtered_by_rating = function (req, res, next) {
    Idea.find({}).sort({'meta.top': -1})
        .exec(function (err, list_ideas) {
            if (err) {
                return next(err);
            }
            //Successful, so render
            res.render('ideas', {title: 'Best rated ideas', idea_listing: list_ideas, user: req.user, top: true});
        });
};


// ################################
// Ideas combining both most comments and best rates
// ################################

exports.idea_filtered_by_comments_and_rating = function (req, res, next) {
    Idea.find({}).sort({'meta.top': -1})
        .exec(function (err, list_ideas) {
            if (err) {
                return next(err);
            }
            //Successful, so render
            res.render('ideas', {
                title: 'Weekly challenge most popular ideas',
                idea_listing: list_ideas,
                user: req.user,
                buzz: true
            });
        });
};


// ########################
// Display form to add an idea
// ############################
exports.ideas_add = function (req, res, next) {
    res.render('addIdea', {title: 'Add your startup', user: req.user});
};


// ############################
// processing form to add an idea
// ############################
exports.ideas_add_process = function (req, res, next) {

    console.log("He ho tatz!");

    var multer = require('multer');

    /*
     var storage = multer.diskStorage({
     destination: function (request, file, callback) {
     callback(null, './public/img/uploads/');
     },
     filename: function (request, file, callback) {
     console.log(file);
     callback(null, file.originalname)
     }
     });
     var upload = multer({storage: storage});

     */

    //Get the parsed information
    var ideaInfo = req.body;
    imageInfo = nomImage;


    if (!ideaInfo.ideaSubject || !ideaInfo.ideaExcerpt || !ideaInfo.ideaDescription ) {

        if (imageInfo = 'undefined') {
            console.log("erreur liée image");
            res.render('addIdea', {message: "Sorry, fill all fields please!", error: "true",  user: req.user});

        } else {
            console.log("erreur liée champs");
            res.render('addIdea', {message: "Sorry, fill all fields please!", error: "true",  user: req.user});
        }
    }
    else {
        console.log("Démarrage save form");
        console.log("Nom image dans form :"+imageInfo);

        name = utilsController.getNameFromDataUser(req);

        var newIdea = new Idea();
        newIdea.ideaSubject = ideaInfo.ideaSubject;
        newIdea.ideaExcerpt = ideaInfo.ideaExcerpt;
        newIdea.ideaDescription = ideaInfo.ideaDescription;
        newIdea.ideaImage = nomImage;
        newIdea.name = name;
        newIdea.idAuthor = req.user.id;
        newIdea.category = ideaInfo.categoryName;
        /*  newIdea.category.push(
         {
         category: ideaInfo.categoryId
         });
         */
        newIdea.save(function (erreur, ideaInfo) {

            console.log("Entrée save idea");

            if (erreur) {
                console.log("erreur liée DB");

                res.render('addIdea', {message: "Database error", error: "true",  user: req.user});
            }
            if (!erreur) {
                console.log("NO ERREUR!");

                res.render('addIdea', {message: "New idea added successfully", success: "true", idea: ideaInfo,  user: req.user});
            }
        });
    }
};
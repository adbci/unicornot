var async = require('async');
var Comment = require('../model/comment');
var User = require('../model/user');
var Idea = require('../model/idea');
var Category = require('../model/category');

// #################################
// Display home page
// #################################
exports.getHomepage = function (req, res, next) {
    res.redirect('/view');
};

// #################################
// Display login page
// #################################
exports.getLoginPage = function (req, res, next) {
    res.render('login', {
        message: req.flash('loginMessage')
    });
};

// #################################
// Display logged user profile
// #################################
exports.getProfile = function (req, res, next) {
    res.render('profile', {
        user: req.user
    });
};

// #################################
// Unlink local user account informations
// #################################
exports.unlinkLocal = function (req, res, next) {
    var user = req.user;
    user.local.email = undefined;
    user.local.password = undefined;
    user.save(function (err) {
        res.redirect('/profile');
    });
};

// #################################
// Unlink google user account informations
// #################################
exports.unlinkGoogle = function (req, res, next) {
    var user = req.user;
    user.google.token = undefined;
    user.save(function (err) {
        res.redirect('/profile');
    });
};

// #################################
// Unlink facebook user account informations
// #################################
exports.unlinkFacebook = function (req, res, next) {
    var user = req.user;
    user.facebook.token = undefined;
    user.save(function (err) {
        res.redirect('/profile');
    });
};

// #################################
// Unlink github user account informations
// #################################
exports.unlinkGithub = function (req, res, next) {
    var user = req.user;
    user.github.token = undefined;
    user.save(function (err) {
        res.redirect('/profile');
    });
};

// #################################
// Unlink twitter user account informations
// #################################
exports.unlinkTwitter = function (req, res, next) {
    var user = req.user;
    user.twitter.token = undefined;
    user.save(function (err) {
        res.redirect('/profile');
    });
};



// the callback after local system has authorized the user
exports.loginLocal = function (req, res, next) {
    res.render('signupLocal', {message: req.flash('loginMessage')});
};

// the callback after local system has authorized the user
exports.callbackLocal = function (req, res, next) {
    passport.authenticate('local-signup', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/connect/local', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    });
};


// the callback after google has authorized the user
exports.callbackGoogle = function (req, res, next) {
    passport.authorize('google', {
        successRedirect: '/profile',
        failureRedirect: '/'
    });
};



// #################################
// processing form to add a comment
// #################################
exports.logout = function (req, res, next) {
    req.logout();
    res.redirect('/index');
};

// #################################
// getting name for req.user object
// #################################
exports.getNameFromDataUser = function (req) {
    var dataUser = req.user;
    var datasToString = dataUser.toString();

    if (datasToString.indexOf('local') > -1) {
        name = req.user.local.name;
    } else if (datasToString.indexOf('facebook') > -1) {
        name = req.user.facebook.name;
    } else if (datasToString.indexOf('twitter') > -1) {
        name = req.user.twitter.name;
    } else if (datasToString.indexOf('google') > -1) {
        name = req.user.google.name;
    } else if (datasToString.indexOf('github') > -1) {
        name = req.user.github.name;
    }

    return name;
};

// #################################
// getting photo for req.user object
// #################################
exports.getPhotoFromDataUser = function (req) {
    var dataUser = req.user;
    var datasToString = dataUser.toString();

    if (datasToString.indexOf('local') > -1) {
        photo = req.user.local.photo;
    } else if (datasToString.indexOf('facebook') > -1) {
        photo = req.user.facebook.photo;
    } else if (datasToString.indexOf('twitter') > -1) {
        photo = req.user.twitter.photo;
    } else if (datasToString.indexOf('google') > -1) {
        photo = req.user.google.photo;
    } else if (datasToString.indexOf('github') > -1) {
        photo = req.user.github.photo;
    }

    return photo;
};


// ################################
// Display a particular category by id
// ################################
exports.getCategoryFromId = function (req, res, next) {

    async.parallel({
        categoryName: function (id, callback) {
            Category.find({'_id': id})
                .exec(callback);
        }
        /*,
         comments_details: function (callback) {
         Comments.find({'idIdea': req.params.id})
         .exec(callback);
         },
         comments_counts: function (callback) {
         Comments.count({'idIdea': req.params.id})
         .exec(callback);
         }

         */
    }, function (err, results) {
        if (err) {
            return next(err);
        }

        console.log(results.categoryName.toString());
        return results.categoryName.toString();

        /*
         res.render('detailsIdea', {
         title: 'Details idea',
         idea_details: results.idea_detail
         comments_idea_details: results.comments_details,
         comments_counts_details: results.comments_counts
         });
         */

    })
};